# LIS4381 Overview - Mobile Web App Development

## Matthew Stoklosa

### Course Work Links Assignments:

1. [A1 README.md](https://bitbucket.org/mes17/lis4381/src/master/a1/ "A1 README.md")

    * Install AMPPS
    * Install JDK
    * Install Android Studio and create My First App
    * Provide screenshots of installations
    * Create Bitbucket repo
    * Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
    * Provide git command descriptions

2. [A2 README.md](https://bitbucket.org/mes17/lis4381/src/master/a2/ "A2 README.md")

    * Create a mobile app inside of Android Studio that has 2 interfaces and displays a recipe
    * Provided screenshots for first and second screens for the user interface
    * Provide screenshots for Skill Sets 1-3

3. [A3 README.md](https://bitbucket.org/mes17/lis4381/src/master/a3/ "A3 README.md")

    * Create a mobile app inside Android Studio that has 2 interfaces and calculates the price of concert tickets
    * Provide screenshots for ERD of a petstore, pet, customer
    * Provide screenshots for Skill Sets 4-6

4. [A4 README.md](https://bitbucket.org/mes17/lis4381/src/master/a4/ "A4 README.md")

    * Create a Bootstrap carousel
    * Provide screenshots for the carousel and data validation being valid and invalid
    * Provide screenshots for Skill Sets 10-12

5. [A5 README.md](https://bitbucket.org/mes17/lis4381/src/master/a5/ "A5 README.md")

    * Develop server-side validation of a form in php
    * Develop a form that adds data into a database
    * Provide screenshots for Skill Sets 13-15

6. [P1 README.md](https://bitbucket.org/mes17/lis4381/src/master/p1/ "P1 README.md")

    * Create a mobile app inside Android Studio that has 2 interfaces that displays itself as a business card
    * Provide screenshots for the first and second screens for the user interface
    * Provide screenshots of skill set 7-9

7. [P2 README.md](https://bitbucket.org/mes17/lis4381/src/master/p2/ "P2 README.md")

    * Add edit and delete functionality to the pet store database that was made in a previous assignment
    * Provide screenshots for the pet store, editing a record in the pet store, and creating an RSS feed