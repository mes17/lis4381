# LIS 4381

## Matthew Stoklosa

### Project 1 Requirements:

## Six Parts

1. Create a launcher icon image and display it in both activities (screens)
2. Add background color to both activities
3. Add border image around image and button
4. Add text shadow to button text
5. Chapter Questions (chs.7 & 8)
6. Skill Set 7-9

#### README.md file should include the following items:

* Screenshot of application running first user interface
* Screenshot of application running second user interface
* Screenshot of Skills set running

#### Assignment Screenshots:

| *Screenshot of Android App First Interface*:      | *Screenshot of Android App Second Interface*:   |
|---------------------------------------------------|-------------------------------------------------|
| ![Android App Second Interface](img/android3.png) | ![Android App First Interface](img/android4.png)|

#### Skill Sets Screenshots:

| *Screenshot of Skill set Random Array Using Methods and Data Validation*:|*Screenshot of Skill set Largest of Three Intergers*: | *Screenshot of Skill set Array Runtime Data Validation*:  |
|------------------------------------------------------|-----------------------------------------|-------------------------------------|
| ![Decision Structures](img/Random_Array.png) | ![Random Array](img/LargestofThree.png)  | ![Arrays And Loops](img/Methods.png)|