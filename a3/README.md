# LIS 4381

## Matthew Stoklosa

### Assignment 3 Requirements:

## Four Parts

1. Create database for a pet store
2. Create Android Application that can calculate price for tickets
3. Chapter Questions (chs.5 & 6)
4. Skill Set 4-6

### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of Android App Running First & Second Interface
* Links to following files: a3.mwb and a3.sql

#### Assignment Screenshots:

#### ERD Screenshot:

| *Screenshot of ERD*:      |
|---------------------------|
| ![ERD3](img/a3_ERD_3.png) |

#### Android app Screenshot:

| *Screenshot of Android App First Interface*:      | *Screenshot of Android App Second Interface*:   |
|---------------------------------------------------|-------------------------------------------------|
| ![Android App Second Interface](img/android3.png) | ![Android App First Interface](img/android4.png)|

#### Skill Sets Screenshots:

| *Screenshot of Skill set Decision Structures*:       | *Screenshot of Skill set Random Array*: | *Screenshot of Skill set Methods*:  |
|------------------------------------------------------|-----------------------------------------|-------------------------------------|
| ![Decision Structures](img/DecisionStructures_1.png) | ![Random Array](img/RandomArray_1.png)  | ![Arrays And Loops](img/Methods.png)|

#### Assigmnet Links:

*A3 docs: a3.mwb and a3.sql*:

[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")

[A3 SQL File](docs/a3.sql "A3 SQL Script")
