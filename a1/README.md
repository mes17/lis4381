# LIS 4381

## Matthew Stoklosa

### Assignment 1  Requirements:

1. Distrubeted Bersion Control with Git an dBitbucket
2. Developmemt instructions
3. Chapter Questions (chs.1 & 2)

#### README.md file should include the following items:

* Screenshot of AMPPS installation 
* Screenshot of Java Hello
* Screenshot of running Anmdrord Studios My Frist App
* Git commands with short description 
* Bitbucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstationslocation and myteamquotes)

> #### Git commands w/short descriptions:

1. git init - Creates a new local repository with the specified name
2. git status - Lists all new or modified files to be commited
3. git add - Snapshots the file in preparation for versioning
4. git commit - Records file snapshots permanently in version history  
5. git push - Uploads all local branch commits to GitHub
6. git pull - Downloads bookmark history and incorporates changes
7. git branch - Lists all local branches in the current repository

#### Assignment Screenshots:

| *Screenshot of AMPPS running http://localhost* :  | *Screenshot of AMPPS running http://localhost* :             |
|---------------------------------------------------|--------------------------------------------------------------|
| ![AMPPS Installation Screenshot](img/ampps_rs.png)| ![AMPPS Installation Screenshot](img/ampps_ss.png)           |
|---------------------------------------------------|--------------------------------------------------------------|
| *Screenshot of running java Hello* :              | *Screenshot of Android Studio - My First App* :              |
|---------------------------------------------------|--------------------------------------------------------------|
| ![JDK Installation Screenshot](img/a1_java.png)   | ![Android Studio Installation Screenshot](img/android_s.png) |



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mes17/bitbucketstationlocation/src/ "Bitbucket Station Locations")