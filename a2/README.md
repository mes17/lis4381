# LIS 4381

## Matthew Stoklosa

### Assignment 2 Requirements:
## Five parts

1. Create a mobile recipe app using Android Studio
2. Provide screenshots for first user interface
3. Provide screenshots for second user interface
4. Answer Chapter 3 and 4 Questions
5. Skill set 1-3

#### README.md file should include the following items:

* Course title, Your name, Assignment Requirements
* Screenshot of running applications first user interface
* Screenshot of running applications second user interface
* Screenshot of Skill set Even or Odd
* Screenshot of Skill set Largest Number
* Screenshot of Skill set Arrays And Loops

#### Assignment Screenshots:

| *Screenshot of 1st page of Recipe App*: | *Screenshot of 2nd page of Recipe App*:  |
|-----------------------------------------|------------------------------------------|
| ![1st Recipe App](img/assignment2-1.png)| ![2st Recipe App](img/assignment2-2.png) |

#### Skill Sets Screenshots:

| *Screenshot of Skill set Even or Odd*:   | *Screenshot of Skill set Largest Number*:   | *Screenshot of Skill set Arrays And Loops*:       |
|------------------------------------------|---------------------------------------------|---------------------------------------------------|
| ![Even or Odd](img/EvenOrOdd_1.png)      | ![Largest Number](img/LargestNumber_1.png)  | ![Arrays And Loops](img/ArraysAndLoops_1.png)     |