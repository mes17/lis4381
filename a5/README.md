# LIS 4381

## Matthew Stoklosa

### Assignment 5  Requirements:

## Four Parts

1. Develop server-side validation of a form in php
2. Develop a form that adds data into a database
3. Chapter Questions (chs. 11, 12, & 19)
4. Skill Set 13-15

#### README.md file should include the following items:

* Screenshot of index.php for the data in petstore
* Screenshot of server-side data validation
* Local lis4381 web app: http://localhost/repos/lis4381/
* Screenshot of Skills set running

#### Assignment Screenshots:

|*Screenshot of Index.php* :                      | *Screenshot of add_petstore_process.php that incliudes error.php* : |
|-------------------------------------------------|-----------------------------------------------------------------------|
| ![Screenshot of Index](img/inde3.png)           | ![Screenshot of Error](img/error.png)                                 |


#### Skill Sets Screenshots:

| *Screenshot of Skill set Employee Inherits Person Class*:|
|----------------------------------------------------------|
|      ![Employee Inherits Person Class](img/sn13.png)     |

|*Screenshot of Skill set PHP Simple Calculator Add*: | *Screenshot of Skill set PHP Simple Calculator Add process*: |
|-----------------------------------------------------|--------------------------------------------------------------|
|      ![PHP Simple Calculator](img/calb.png)         |       ![PHP Wirte/Read File](img/answera.png)                |

|*Screenshot of Skill set PHP Simple Calculator Divide*: | *Screenshot of Skill set PHP Simple Calculator Divide Process*: |
|--------------------------------------------------------|-----------------------------------------------------------------|
|      ![PHP Simple Calculator](img/calae.png)           |                     ![PHP Wirte/Read File](img/answerc.png)     |


|*Screenshot of Skill set PHP Wirte/Read File index*:    | *Screenshot of Skill set PHP Wirte/Read File process*: |
|--------------------------------------------------------|--------------------------------------------------------|
|      ![PHP Simple Wirte/Read File index](img/wr1.png)  |        ![PHP Wirte/Read File process](img/wr2.png)     |