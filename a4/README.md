# LIS 4381

## Matthew Stoklosa

### Assignment 4 Requirements:

## Five Parts

1. Create Bootstrap carousel to go into different assignments
2. Create a form to collect data and does client-side validation
3. Link to local lis4381 web app:
4. Chapter Questions (chs.9, 10 & 15)
5. Skill Set 10-12

#### README.md file should include the following items:

* Screenshot of carousel
* Screenshot of Assignment 4 invalid data entry
* Screenshot of Assignment 4 valid data entry
* Local lis4381 web app: http://localhost/repos/lis4381/

#### Assignment Screenshots:

| *Screenshot of carousel*:                    | *Screenshot of Assignment 4 invalid data entry*:|*Screenshot of Assignment 4 valid data entry*:|
|----------------------------------------------|-------------------------------------------------|----------------------------------------------|
| ![Carousel running](img/carouse1.png)        | ![Data Validation](img/dataValidation1.png)     |![Data Validation](img/dataValidation2.png)   |

#### Skill Sets Screenshots:

| *Screenshot of Skill set Array Lists*:|   *Screenshot of Skill set Nested Structures*:  | *Screenshot of Skill set Person Class*: |
|---------------------------------------|-------------------------------------------------|-----------------------------------------|
|   ![Array Lists](img/Array_Lists.png) | ![Nested Structures](img/Nested_Structure2.png) |  ![Person Class](img/Person_Class.png)  |