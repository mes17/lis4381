import java.util.Scanner;
import java.util.Random;

public class Methods
  {

  public static void getRequirements()
  {

    System.out.print("Developer: Matthew Stoklosa\n");
    System.out.println("Program creates array size at run-time");
    System.out.println("Program rounds sum and average of numbers to two decimal places");
    System.out.println("Note: numbers *must* be float data type");
    System.out.println(); //blank line




    // taking array input from user
    Scanner input = new Scanner(System.in);
    System.out.print("Please enter length of String array ");
    float sum=0;
    float avg=0;    


int length = input.nextInt();

    // create an array to save user input
    int[]arr = new int[length];
   
    // loop over array to save user input
    for (int i = 1; i <= length; i++) {

    System.out.print("Enter the number in element" + i + ":");
   float num = input.nextFloat();
   sum += num;
   avg = sum/length;
    }
    System.out.println("\n");
    System.out.printf("The sum is: " + "%.2f",sum);
    System.out.println("\n");
    System.out.printf("The average is: " + "%.2f",avg);
    System.out.println("\n");

   }  
}
