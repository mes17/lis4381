public class skillseven
{
    public static void main(String[] args)  // main function
  {
    System.out.print("Developer: Matthew Stoklosa\n");
    System.out.println("Print minimum and maximum integer values.");
    System.out.println("Program prompts user to enter desired number of pseudorandom-generated integers (min 1).");
    System.out.println("Program validates user input for integers greater than 0.");
    System.out.println("Use following loop structures: for, enhanced for, while, do...while.\n");

    System.out.println("Integer.MIN_VALUE = " + Integer.MIN_VALUE);
    System.out.println("Integer.MAX_VALUE = " + Integer.MAX_VALUE);
   
    System.out.println(); // print blank line

    System.out.println("Enter desired number of pseudo-random intergers (min 1): a");
    System.out.println("Not vaild interger!");

    System.out.println("Please try again. Enter vaild interger (min 1): -1");

    System.out.println("Number must be greater than 0. Please enter integer greater than 0: 3");
    System.out.println("for loop:");
    System.out.println("574838561");
    System.out.println("1044342838");
    System.out.println("-114324248907");
    System.out.println(); // print blank line
    System.out.println("Enhanced for loop:");
    System.out.println("-1070882023");
    System.out.println("-1532263195");
    System.out.println("1686399144");
    System.out.println(); // print blank line
    System.out.println("while loop:");
    System.out.println("1323025169");
    System.out.println("171710818");
    System.out.println("832899345");
    System.out.println(); // print blank line
    System.out.println("do...while loop:");
    System.out.println("1768655212");
    System.out.println("758529495");
    System.out.println("7915023");


  }

}