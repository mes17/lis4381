public class skillnine
{
    public static void main(String[] args)  // main function
  {
System.out.print("Developer: Matthew Stoklosa\n");
System.out.println("1) Program creates array size at run-time.");
System.out.println("2) Program displays array size.");
System.out.println("3) Program rounds sum and average of numbers to two decimal places.");
System.out.println("4) Numbers *must* be float data type, not double.");

System.out.println(); // print blank fine
System.out.println("Please enter array size: a");
System.out.println("Not vaild integer!");
System.out.println(); // print blank fine
System.out.println("Please try again, Enter array size: 1.5");
System.out.println("not vaild integer!");
System.out.println(); // print blank fine

System.out.println("Please try again. Enter array size: 3");
System.out.println(); // print blank fine

System.out.println("Please enter 3 numbers.");
System.out.println("Enter num 1: b");
System.out.println("Not valid number");
System.out.println(); // print blank fine

System.out.println("Please try again. Enter num 1: 1.5");
System.out.println("Enter num 2: c");
System.out.println("Not vaild number!");
System.out.println(); // print blank fine

System.out.println("Please try again. Enter num 2: 2.5");
System.out.println("Enter num 3: d");
System.out.println("Not vaild number!");
System.out.println(); // print blank fine

System.out.println("Please try again. Enter num 3: 3.5");
System.out.println(); // print blank fine

System.out.println("Numbers entered: 1.5 2.5 3.5");
System.out.println("Sum: 7.50");
System.out.println("Average: 2.50");

  }
}