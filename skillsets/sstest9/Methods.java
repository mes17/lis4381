























import java.util.Scanner;

class Methods
{
//creat2 global Scanne: object, used in more than one miethod
//Note: using “final” prevents object variable from being modifed
static final Scanner sc = new Scanner(System.in);

//nonvalue-raturning method (static reauires no obj=ct)
public static void getRequirements()
{
// display operational inessages
System.out.println("1) Program creates array size at run-time.");
System.out.println("2) Program displays array size.");
System.out.println("3) Program rounds sum and average of numbers to two decimal places.");
System.out.println("4) Numbers *must* be float data type, not double.");

System.out.println(); // print blank fine
}

//value-returning method (static requires no object)
public static int validateArraySize()
{
//declare variables and create Scanner object
//Scanner sc = new Scanner(System.in);
int arraySize = 0;

//prompt user for array size
System.out.print("Please enter array size: ");
while (!sc.hasNextInt())
{
System.out.println("Not valid integer!\n");
sc.next();//Important! If omitted, will go into infinite loop on invalid input!
System.out.print("Please try again. Enter array size: ");
}
arraySize = sc.nextInt();
System.out.println();


return arraySize;
}


public static void calculateNumbers(int arraySize)
{
float sum = 0.0f;
float average = 0.0F;

//indicate number of values required, based upon user input
System.out.print("Please enter " + arraySize + " numbers.\n");

//create array for storing user input, based upon user-entered array size
float numsArray[] = new float[arraySize];

//validate data entry
for(int i = 0; i < arraySize ; i++)
{
System.out.print("Enter num " + (i+ 1) +": ");

while (!sc.hasNextFloat())
{
System.out.println("Not valid number!\n");
sc.next();//Important! If omitted, will go into infinite loop on invalid input!
System.out.print("Please try again. Enternum "+ (i +1) +": ");
}
numsArray[i] = sc.nextFloat(); //capture validated user input
sum = sum + numsArray[i]; //process data entry
}
average = sum / arraySize;

//print numbers entered
System.out.print("\nNumbers entered: ");
for (int i = 0; i < numsArray.length; i++)
System.out.print(numsArray[i]+" ");

//call method to print and format numbers
printNumbers(sum, average);
}


public static void printNumbers(float sum, float average)
{

    System.out.println("\nSum: "+ String.format("%.2f", sum));
    System.out.println("Average: "+ String.format("%.2f", average));
}
}