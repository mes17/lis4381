import java.util.Scanner;
import java.util.Random;

class Methods

{
		public static void  displayProgram()
		{

    //display operational messages
    System.out.print("Developer: Matthew Stoklosa\n");
	System.out.print("Program prompts user to enter desired number of pseudorandom-generated integers (min 1) ");
	System.out.print("Use following loop structures for, enhanced for, while, do...while ");
	System.out.println(); //print blank line
	    }
	
	public static int[] createArray()
	{
	
    //declare variables and create Scanner object
    int arraySize;
    Scanner input = new Scanner(System.in);
    
    System.out.print("Enter desired number of pseudo-random integers: ");
    while(!input.hasNextInt())
    {
       System.out.println("Not valid integer!\n");
       input.next(); //IMPORTANT IF OMMITTED WILL GO INTO INFINITE LOOP on invalid input!!
	   System.out.print("Please try again. Enter desired number of pseudo-random integers: ");
	}
	
	arraySize = input.nextInt();
	return arraySize;
	}
	

	public static void displayArrays(int num)
	{
	
	 random shuffle = new Random(); //instantiate random object
	 int i = 0;
	
	
	 //java style String[] myArray
	 //C++ style String myArray[]
	 int myArray[] = new int[num];


//for loop

        System.out.println("for loop:");

        for(int i=0; i < myArray.length; i++)
        {

                System.out.println(shuffle.nextInt());
        }
 //enhanced for loop       

	System.out.println("\nEnhanced for loop:");
        for(int n: myArray)
        {
                System.out.println(shuffle.nextInt());
        }
        
  //while loop      

    System.out.println("\nwhile loop:");
        
        int i=0;
        
        while (i<myArray.length)
                {
                System.out.println(shuffle.nextInt());
                i++;
                }
	//do while
        i=0;
        System.out.println("\ndo...while loop:");

        do
        {
                System.out.println(shuffle.nextInt());
                i++;
        } while (i < myArray.length);
    }
    
    
public static void main(String args[])
{
    //call void method: display operational messages
    displayProgram();
    
    //call value- returning method: get array size
    int arrayNum = getArraySize();
    
    //call void method print arrays
    displayArrays(arrayNum);

	} 
	
}
