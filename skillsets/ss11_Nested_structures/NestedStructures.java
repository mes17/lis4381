import java.util.Scanner; //imports all libraries

public class NestedStructures //declared class
{
  public static void main(String[] args)
  {
    Scanner in = new Scanner(System.in); //allows user input
    int[] array = {3, 99, -1, 3, 7};
    int input = 0;

    //Code starts here
    System.out.print("Developer: Matthew Stoklosa\n");
    System.out.print("Program searches user entered integer w/in array of integers.\n");
    System.out.print("Create an array with the following values: 3, 99, -1, 3, 7 \n\n");

    System.out.println("Array length (min 1): " + array.length);
    System.out.print("Enter search value: ");

    input = in.nextInt();  //user input

    for(int i = 0; i < array.length; i++)
    {
      if(input == array[i])
      {
      System.out.println(input + " found at index " + i);
      }
      else if(input != array[i])
      {
      System.out.println(input + " *not* found at index " + i);
      }
    }




  }
}
