import java.util.Scanner;

public class Methods
{
//Create Method without returning any value (without object)
public static void getRequirements()
{
    System.out.print("Developer: Matthew Stoklosa\n");
    System.out.println("Program prompts user for first name and age, then prints results");
    System.out.println("Creat four methods form the following requirements:");
    System.out.println("1) getRequirements(): Void methods displays program requirements.");
    System.out.println("2) getUserInput(): Void method prompts for user input, \n\tthen  calls two methods: myVoidMethod() and myValueReturningMethod().");
    System.out.println("3) my VoidMethod():\n" +
                        "\ta. Accepts two agruments: String and int. \n" +
                        "\tb. Prints user's first name and age");
    System.out.println("4) myValueReturningMethod():\n" +
                        "\ta. Accepts two arguments: String and int. \n" +
                        "\tb. Returins String containg first name age.");

    System.out.println();
}

public static void getUserInput()
{
// initialize variables, create Scanner object, capture user input
String firstName="";
int userAge = 0;
String myStr="";
Scanner sc = new Scanner(System.in);

//input
System.out.print("Enter first name: ");
firstName=sc.next();

System.out.print("Enter age: ");
userAge = sc.nextInt();

System.out.println();

//Note: done for simplicity--method/function calls *should*® go back to their calling environment
//call void method R
System.out.print("void method call: ");
myVoidMethod(firstName, userAge);

//call value-returning method
System.out.print("value-returing method call: ");
myStr = myValueReturningMethod(firstName, userAge);
System.out.println(myStr);
}

//Note: both methods use *same* named parameters--wl¥ch are *local* variables!
//Also, both methods are static--that is, can be used w/o instantiating objects
public static void myVoidMethod(String first, int age)
{
    System.out.println(first + " is " + age);
}

public static String myValueReturningMethod(String first, int age)
{
//note: implicit string conversion of age (int)
return first + " is " + age;
}
}